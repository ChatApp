/*
 * © 2010 ROBO Design
 * http://www.robodesign.ro
 *
 * $Date: 2010-05-19 18:46:10 +0300 $
 */

import ChatApp.appMain;
import javax.swing.SwingUtilities;

public class ChatApp {
  public static void main (String[] args) {
    appMain app = new appMain();

    if (args != null && args.length > 0 && args[0].equals("--debug")) {
      app.debug = true;
    }

    SwingUtilities.invokeLater(app);
  }
}

// vim:set fo=wancroql tw=80 ts=2 sw=2 sts=2 sta et ai cin ff=unix:

