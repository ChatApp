/*
 * © 2010 ROBO Design
 * http://www.robodesign.ro
 *
 * $Date: 2010-05-19 18:46:37 +0300 $
 */

package ChatApp;

import java.net.ServerSocket;
import java.util.concurrent.ConcurrentHashMap;
import javax.swing.SwingUtilities;

// clasa ce se ocupa de gazduirea unui server de chat.

public class appServer extends Thread {
  protected appMain myApp;
  private volatile int port = 0;
  private final int portMin = 1024;
  private final int portMax = 65535;
  private volatile boolean serverPornit = false;
  private ServerSocket srvSocket;
  private ConcurrentHashMap<String, appServerThread> listaClienti;

  public appServer (appMain rootApp) {
    myApp = rootApp;
    listaClienti = new ConcurrentHashMap<String, appServerThread>();
  }

  // pornirea serverului
  public boolean pornire (int p) {
    myApp.mesajDebug("appServer.pornire " + p);

    if (serverPornit || srvSocket != null || p < portMin || p > portMax) {
      System.err.println("appServer.pornire: serverul este pornit deja");
      return false;
    }

    if (!listaClienti.isEmpty()) {
      System.err.println("appServer.pornire: !listaClienti.isEmpty()");
      listaClienti.clear();
    }

    try {
      srvSocket = new ServerSocket(p);
      myApp.mesajDebug("appServer.pornire: srvSocket ready port " + p);
    } catch (Exception ex) {
      System.err.println("appServer.pornire: eroare pornire server. " + ex);
      ex.printStackTrace();
      return false;
    }

    port = p;
    serverPornit = true;
    start();

    return true;
  }

  // functia care ruleaza pe thread separat: ascultarea de conexiuni
  public void run () {
    if (!serverPornit || srvSocket == null || srvSocket.isClosed()) {
      return;
    }

    appServerThread thread = null;
    String nume = "";

    myApp.mesajDebug("appServer.run start");

    // asculta la infinit conexiuni
    while (serverPornit && !srvSocket.isClosed()) {
      nume = "client-" + (System.currentTimeMillis()/1000) + "-" + listaClienti.size();

      try {
        thread = new appServerThread(nume, srvSocket.accept(), this);
      } catch (Exception ex) {
        System.err.println("appServer.run a esuat. " + ex);
        ex.printStackTrace();
        break;
      }

      if (thread != null && !thread.getEroare()) {
        listaClienti.put(nume, thread);
        thread.start();
      }

      thread = null;
    }

    myApp.mesajDebug("appServer.run end");

    if (serverPornit) {
      oprire();
    }
  }

  // aceasta metoda proceseaza comenzile primite de la clienti. metoda este 
  // chemata de threadurile fiecarui client (appServerThread).
  protected boolean procesareComanda (String cmd, appServerThread t) {
    if (!serverPornit) {
      System.err.println("appServer.procesareComanda: serverul nu este pornit.");
      return false;
    }

    String[] arg = cmd.split(" ");
    String fn = arg[0]; // functia, primul element

    myApp.mesajDebug("appServer.procesareComanda " + t.getNumeUtilizator() + " fn " + fn + " cmd " + cmd);

    if ("/conectare".equals(fn) && arg.length == 2) {
      ev_conectareClient(cmd, t, arg[1]);
    } else if ("/deconectare".equals(fn) && arg.length == 2) {
      ev_deconectareClient(cmd, t, arg[1]);
    } else if ("/mesaj".equals(fn) && arg.length > 2) {
      ev_mesajPrimit(cmd, t, arg[1], arg[2]);
    } else if ("/nume".equals(fn) && arg.length == 3) {
      ev_schimbareNume(cmd, t, arg[1], arg[2]);
    } else if ("/lista".equals(fn) && arg.length == 1) {
      ev_listaClienti(t);
    } else {
      return false;
    }

    return true;
  }

  // aceasta metoda trimite o comanda la toti clientii.
  private synchronized boolean trimiteComanda (String cmd) {
    if (!serverPornit || srvSocket == null || srvSocket.isClosed() || listaClienti.isEmpty()) {
      System.err.println("appServer.trimiteComanda: starea serverului este invalida.");
      return false;
    }

    myApp.mesajDebug("appServer.trimiteComanda " + cmd);

    for (appServerThread t : listaClienti.values()) {
      t.trimiteComanda(cmd);
    }

    return true;
  }

  // urmeaza o serie de metode ce sunt chemate atunci cand serverul primeste 
  // comenzi de la clienti.

  private void ev_conectareClient (String cmd, appServerThread t, String nume) {
    if (t.getClientInitializat()) {
      System.err.println("appServer.ev_conectareClient(" + nume + ") clientul este initializat deja");
      return;
    }

    if (nume.equals("#server") || listaClienti.containsKey(nume)) {
      myApp.mesajDebug("appServer.ev_conectareClient(" + nume + ") clientul incearca sa foloseasca un nume existent");
      t.trimiteComanda("/conflict-nume");
      return;
    }

    myApp.mesajDebug("appServer.ev_conectareClient " + nume);

    String numeVechi = t.getNumeUtilizator();
    if (!numeVechi.equals(nume)) {
      t.setNumeUtilizator(nume);
    }
    t.setClientInitializat(true);

    if (listaClienti.containsKey(numeVechi)) {
      listaClienti.remove(numeVechi);
    }

    listaClienti.put(nume, t);

    // retrimitem comanda la toti ceilalti clienti
    trimiteComanda(cmd);

    // la acest clienti mai trimitem si lista actuala de clienti
    String listaCmd = getListaClientiTextual();
    if (!listaCmd.isEmpty()) {
      t.trimiteComanda(listaCmd);
    }
  }

  private void ev_deconectareClient (String cmd, appServerThread t, String nume) {
    if (!t.getClientInitializat() || nume.equals("#server") || !listaClienti.containsKey(nume)) {
      System.err.println("appServer.ev_deconectareClient(" + nume + ") stare invalida");
      return;
    }

    myApp.mesajDebug("appServer.ev_deconectareClient " + nume);

    String numeThread = t.getNumeUtilizator();
    if (!numeThread.equals(nume) || !listaClienti.containsKey(numeThread)) {
      return;
    }

    t.setClientInitializat(false);
    t.oprire();

    if (!listaClienti.isEmpty()) {
      trimiteComanda(cmd);
    }
  }

  private void ev_schimbareNume (String cmd, appServerThread t, String numeVechi, String numeNou) {
    if (numeVechi.equals("#server") || numeNou.equals("#server") || numeNou.equals(numeVechi) || !t.getClientInitializat() || !listaClienti.containsKey(numeVechi) || listaClienti.containsKey(numeNou)) {
      return;
    }

    String numeThread = t.getNumeUtilizator();
    if (!numeThread.equals(numeVechi)) {
      System.err.println("appServer.ev_schimbareNume numeThread != numeVechi " + numeVechi);
      return;
    }

    t.setNumeUtilizator(numeNou);

    listaClienti.remove(numeVechi);
    listaClienti.put(numeNou, t);

    trimiteComanda(cmd);
  }

  private void ev_listaClienti (appServerThread t) {
    String nume = t.getNumeUtilizator();
    if (!t.getClientInitializat() || !listaClienti.containsKey(nume)) {
      System.err.println("appServer.ev_listaClienti: clientul " + nume + " nu este initializat.");
      return;
    }

    myApp.mesajDebug("appServer.ev_listaClienti pentru " + nume);

    String listaCmd = getListaClientiTextual();
    if (!listaCmd.isEmpty()) {
      t.trimiteComanda(listaCmd);
    }
  }

  private void ev_mesajPrimit (String cmd, appServerThread t, String dela, String catre) {
    String nume = t.getNumeUtilizator();

    myApp.mesajDebug("appServer.ev_mesajPrimit(" + nume + ") dela " + dela + " catre " + catre);

    if (!t.getClientInitializat() || !listaClienti.containsKey(nume) || !nume.equals(dela) || catre.equals(dela) || (!catre.equals("#server") && !listaClienti.containsKey(catre))) {
      System.err.println("appServer.ev_mesajPrimit(" + nume + "): clientul nu este initializat sau a trimis un mesaj invalid.");
      return;
    }

    if (catre.equals("#server")) {
      trimiteComanda(cmd);
    } else {
      appServerThread catreThread = listaClienti.get(catre);
      catreThread.trimiteComanda(cmd);
      t.trimiteComanda(cmd);
    }
  }

  // aceasta metoda returneaza lista de clienti, sub forma de comanda, pentru a 
  // fi trimisa la client.
  private synchronized String getListaClientiTextual () {
    if (listaClienti.isEmpty()) {
      return "";
    }

    StringBuilder cmd = new StringBuilder();
    cmd.append("/lista");
    for (String nume : listaClienti.keySet()) {
      cmd.append(" " + nume);
    }

    return cmd.toString();
  }

  // metoda este chemata atunci cand un client se opreste.
  protected synchronized void clientIesire (appServerThread t) {
    myApp.mesajDebug("appServer.clientIesire " + t.getNumeUtilizator());

    if (listaClienti.containsKey(t.getNumeUtilizator())) {
      listaClienti.remove(t.getNumeUtilizator());
    }

    myApp.mesajDebug("appServer.clientIesire listaClienti.size() = " + listaClienti.size());

    if (serverPornit && t.getClientInitializat()) {
      trimiteComanda("/deconectare " + t.getNumeUtilizator());
    }

    if (serverPornit && listaClienti.isEmpty()) {
      myApp.mesajDebug("appServer.clientIesire oprire server");

      if (srvSocket != null && !srvSocket.isClosed()) {
        try {
          srvSocket.close();
        } catch (Exception ex) {
          System.err.println("appServer.oprire: srvSocket.close() exception. " + ex);
          ex.printStackTrace();
        }
      }

      srvSocket = null;
      port = 0;
      serverPornit = false;

      SwingUtilities.invokeLater(new Runnable () {
        public void run () {
          myApp.deconectareServerCompleta();
        }
      });
    }
  }

  // oprire server
  public void oprire () {
    if (!serverPornit) {
      return;
    }

    myApp.mesajDebug("appServer.oprire");

    trimiteComanda("/deconectare #server");

    serverPornit = false;
  }

  public boolean getServerPornit () {
    return serverPornit;
  }

  public int getPort () {
    return port;
  }
}

// vim:set fo=wancroql tw=80 ts=2 sw=2 sts=2 sta et ai cin ff=unix:
