/*
 * © 2010 ROBO Design
 * http://www.robodesign.ro
 *
 * $Date: 2010-05-19 18:47:29 +0300 $
 */

package ChatApp;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.UIManager;

// codul principal de gestionare a rularii aplicatiei ChatApp
public class appMain implements Runnable {
  public static boolean debug = false;
  private PrimaPagina primapag;
  private PaginaChat chatpag;
  private volatile String paginaActiva = "";
  private JFrame myFrame;
  private JRootPane rootPane;
  private appServer serverLocal;
  private appClient clientServer;

  // initializarea aplicatiei
  public void run () {
    mesajDebug("appMain.run pornire");

    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } catch (Exception e) {
      System.err.println("Nu am reusit sa schimb interfata.");
    }

    myFrame = new JFrame("ChatApp");
    myFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    myFrame.addWindowListener(iesireAppListener);
    rootPane = myFrame.getRootPane();

    serverLocal = new appServer(this);
    clientServer = new appClient(this);
    primapag = new PrimaPagina(this);
    chatpag = new PaginaChat(this);

    afisarePrimaPagina();
    myFrame.setVisible(true);
  }

  private void afisarePrimaPagina () {
    if (!paginaActiva.equals("PrimaPagina")) {
      primapag.initializare();
      paginaActiva = "PrimaPagina";
    }
  }

  protected void afisarePaginaChat () {
    if (!paginaActiva.equals("PaginaChat")) {
      chatpag.initializare();
      paginaActiva = "PaginaChat";
    }
  }

  // metoda porneste serverul local de gazduire chat.
  public boolean startServer (String nick, int port) {
    mesajDebug("appMain.startServer nick " + nick + " port " + port);

    if (getServerPornit()) {
      if (getServerPort() == port) {
        return true;
      } else {
        System.err.println("appMain.startServer: serverul este pornit deja pe portul " + getServerPort() + ".");
        JOptionPane.showMessageDialog(myFrame, "Serverul este pornit deja pe portul" + getServerPort() + ".", "Eroare pornire server - ChatApp", JOptionPane.ERROR_MESSAGE);
        return false;
      }
    }

    if (serverLocal.pornire(port)) {
      return conectareServer(nick, "localhost", port);
    } else {
      System.err.println("appMain.startServer: pornirea serverului a esuat.");
      JOptionPane.showMessageDialog(myFrame, "Pornirea serverului a esuat.", "Eroare pornire server - ChatApp", JOptionPane.ERROR_MESSAGE);
      return false;
    }
  }

  // metoda permite conectarea la un server de chat.
  public boolean conectareServer (String nick, String server, int port) {
    mesajDebug("appMain.conectareServer nick " + nick + " server " + server + " port " + port);

    if (getServerConectat()) {
      if (nick.equals(getNumeUtilizator()) && server.equals(getServerAdresa()) && port == getServerPort()) {
        return true;
      } else {
        System.err.println("appMain.conectareServer a esuat: exista o conexiune deja facuta catre alt server.");
        JOptionPane.showMessageDialog(myFrame, "Aplicatia este conectata deja la un alt server.", "Eroare conectare server - ChatApp", JOptionPane.ERROR_MESSAGE);
        return false;
      }
    }

    if (!clientServer.conectare(nick, server, port)) {
      System.err.println("appMain.conectareServer a esuat: " + nick + " " + server + ":" + port);
      JOptionPane.showMessageDialog(myFrame, "Conectarea la server a esuat.", "Eroare conectare server - ChatApp", JOptionPane.ERROR_MESSAGE);
      return false;
    }

    return true;
  }

  // metoda permite deconectarea de la serverul de chat.
  public void deconectareServer () {
    mesajDebug("appMain.deconectareServer");
    if (getServerPornit()) {
      serverLocal.oprire();
    }
  }

  protected void deconectareServerCompleta () {
    mesajDebug("appMain.deconectareServerCompleta");
    serverLocal = null;
    serverLocal = new appServer(this);
  }

  // metoda permite deconectarea clientului de la serverul de chat.
  public void deconectareClient () {
    mesajDebug("appMain.deconectareClient");
    if (getServerConectat()) {
      clientServer.deconectare();
    }
  }

  protected void deconectareClientCompleta () {
    mesajDebug("appMain.deconectareClientCompleta");
    clientServer = null;
    clientServer = new appClient(this);
  }

  protected void deconectareGUI () {
    mesajDebug("appMain.deconectareGUI");

    deconectareClient();
    if (paginaActiva.equals("PaginaChat")) {
      afisarePrimaPagina();
    }
    deconectareServer();
  }

  // aceasta metoda permite utilizatorului sa trimita un mesaj
  public boolean trimiteMesaj (String catre, String msg) {
    if (!getServerConectat() || msg == null || msg.trim().isEmpty() || catre == null || catre.isEmpty() || catre.equals(getNumeUtilizator()) || catre.indexOf(" ") != -1 || (!catre.equals("#server") && !getListaClienti().contains(catre))) {
      return false;
    }

    if (clientServer.trimiteMesaj(catre, msg)) {
      if (paginaActiva.equals("PaginaChat")) {
        chatpag.ev_mesajTrimis(catre, msg);
      }
      return true;
    } else {
      return false;
    }
  }

  // metoda permite utilizatorului sa-si schimbe numele
  public boolean schimbareNume (String numeNou) {
    if (!getServerConectat() || numeNou == null || numeNou.equals("#server") || numeNou.trim().isEmpty() || numeNou.indexOf(" ") != -1 || getListaClienti().contains(numeNou)) {
      return false;
    }

    return clientServer.schimbareNume(numeNou);
  }

  // urmeaza o serie de metode ce sunt invocate atunci cand sunt primite comenzi 
  // de la server, de catre clientul conectat.

  protected void ev_conectareClient (String nume) {
    mesajDebug("appMain.ev_conectareClient " + nume);
    if (paginaActiva.equals("PaginaChat")) {
      chatpag.ev_conectareClient(nume);
    } else {
      System.err.println("appMain.ev_conectareClient: PaginaChat nu este activa.");
    }
  }

  protected void ev_deconectareClient (String nume) {
    mesajDebug("appMain.ev_deconectareClient " + nume);
    if (paginaActiva.equals("PaginaChat")) {
      chatpag.ev_deconectareClient(nume);
      if (nume.equals(getNumeUtilizator())) {
        afisarePrimaPagina();
      }
    } else {
      System.err.println("appMain.ev_deconectareClient: PaginaChat nu este activa.");
    }
  }

  protected String ev_conflictNume () {
    String numeNou = (String) JOptionPane.showInputDialog(myFrame, "Numele folosit este luat deja.\nNume nou:", "Conflict nume - ChatApp", JOptionPane.PLAIN_MESSAGE, null, null, getNumeUtilizator());
    if (numeNou == null || numeNou.isEmpty() || numeNou.equals(getNumeUtilizator())) {
      return null;
    }

    numeNou = numeNou.trim();
    if (!numeNou.isEmpty()) {
      numeNou = numeNou.replaceAll("/\\s+/", "-");
    }

    if (numeNou.equals("#server")) {
      JOptionPane.showMessageDialog(myFrame, "Noul nume este invalid.", "Eroare schimbare nume - ChatApp", JOptionPane.ERROR_MESSAGE);
      return ev_conflictNume();
    }

    return numeNou;
  }

  protected void ev_schimbareNume (String numeVechi, String numeNou) {
    if (paginaActiva.equals("PaginaChat")) {
      chatpag.ev_schimbareNume(numeVechi, numeNou);
    } else {
      System.err.println("appMain.ev_deconectareClient: PaginaChat nu este activa.");
    }
  }

  protected void ev_mesajPrimit (String dela, String catre, String msg) {
    mesajDebug("appMain.ev_mesajPrimit dela " + dela + " catre " + catre);

    if (paginaActiva.equals("PaginaChat")) {
      chatpag.ev_mesajPrimit(dela, catre, msg);
    } else {
      System.err.println("appMain.ev_mesajPrimit: PaginaChat nu este activa.");
    }
  }

  protected void ev_listaClienti () {
    mesajDebug("appMain.ev_listaClienti");

    if (paginaActiva.equals("PaginaChat")) {
      chatpag.ev_listaClienti();
    } else {
      System.err.println("appMain.ev_listaClienti: PaginaChat nu este activa.");
    }
  }

  // event handler pentru inchiderea ferestrei.
  private WindowListener iesireAppListener = new WindowAdapter () {
    public void windowClosing (WindowEvent ev) {
      iesireApp();
    }
  };

  public void iesireApp () {
    mesajDebug("appMain.iesireApp");
    deconectareClient();
    deconectareServer();
    System.exit(0);
  }

  protected JFrame getMyFrame () {
    return myFrame;
  }

  protected JRootPane getRootPane () {
    return rootPane;
  }

  public String getServerAdresa () {
    return clientServer.getServerAdresa();
  }

  public int getServerPort () {
    return clientServer.getServerPort();
  }

  public boolean getServerPornit () {
    return serverLocal.getServerPornit();
  }

  public boolean getServerConectat () {
    return clientServer.getServerConectat();
  }

  public String getNumeUtilizator () {
    return clientServer.getNumeUtilizator();
  }

  protected DefaultListModel getListaClienti () {
    return clientServer.getListaClienti();
  }

  public static String stringArrayJoin (String[] arr) {
    return appMain.stringArrayJoin(arr, null, 0, arr.length-1);
  }

  public static String stringArrayJoin (String[] arr, String delim) {
    return appMain.stringArrayJoin(arr, delim, 0, arr.length-1);
  }

  public static String stringArrayJoin (String[] arr, String delim, int offsetStart) {
    return appMain.stringArrayJoin(arr, delim, offsetStart, arr.length-1);
  }

  public static String stringArrayJoin (String[] arr, String delim, int offsetStart, int offsetEnd) {
    StringBuilder sb = new StringBuilder();
    for (int i = offsetStart; i <= offsetEnd; i++) {
      sb.append(arr[i]);
      if (delim != null) {
        sb.append(delim);
      }
    }
    return sb.toString();
  }

  public static void mesajDebug (String msg) {
    if (appMain.debug) {
      System.out.println(msg);
    }
  }
}

// vim:set fo=wancroql tw=80 ts=2 sw=2 sts=2 sta et ai cin ff=unix:
