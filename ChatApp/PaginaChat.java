/*
 * © 2010 ROBO Design
 * http://www.robodesign.ro
 *
 * $Date: 2010-05-19 18:52:50 +0300 $
 */

package ChatApp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicButtonUI;

public class PaginaChat extends JPanel implements ActionListener, ChangeListener {
  private appMain myApp;
  private JLabel infoClienti;
  private JLabel infoConexiune;
  private JButton btnNume;
  private JButton btnDeconectare;
  private JButton btnTrimite;
  private JList listaClienti;
  private JTextField msgField;
  private JButton btnTrimiteMesaj;
  private JTabbedPane chatTabs;
  private PanouMesaje mesajeServer = null;
  private JSplitPane tabServer;

  public PaginaChat (appMain rootApp) {
    super(new GridBagLayout());

    myApp = rootApp;

    infoConexiune = new JLabel("Conectat la server:port.");

    // Numele utilizatorului este afisat cu un buton ce permite utilizatorului 
    // sa-si schimbe numele daca este apasat.
    btnNume = new JButton("Nume:");
    btnNume.setToolTipText("Clic pentru schimbarea numelui tau");
    btnNume.setDefaultCapable(false);
    btnNume.setActionCommand("schimbareNume");
    btnNume.addActionListener(this);

    // Deconectarea de la server.
    btnDeconectare = new JButton("Deconectare");
    btnDeconectare.setDefaultCapable(false);
    btnDeconectare.setActionCommand("deconectare");
    btnDeconectare.addActionListener(this);

    // Trimitere de mesaje.
    btnTrimite = new JButton("Trimite");
    btnTrimite.setDefaultCapable(false);
    btnTrimite.setActionCommand("trimiteMesaj");
    btnTrimite.addActionListener(this);

    msgField = new JTextField();
    msgField.setActionCommand("trimiteMesaj");
    msgField.addActionListener(this);

    // lista de clienti conectati la server.

    listaClienti = new JList();
    infoClienti = new JLabel("Nici un client", JLabel.CENTER);
    infoClienti.setLabelFor(listaClienti);
    listaClienti.setLayoutOrientation(JList.VERTICAL);
    listaClienti.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    listaClienti.addMouseListener(listaClientiMouseListener);
    listaClienti.addKeyListener(fieldKeyListener);

    JScrollPane scrollListaClienti = new JScrollPane(listaClienti);

    listaClienti.ensureIndexIsVisible(0);

    // panoul cu lista de clienti.
    JPanel panouClienti = new JPanel(new GridBagLayout());
    panouClienti.setMinimumSize(new Dimension(100, 100));

    GridBagConstraints c = new GridBagConstraints();

    c.insets = new Insets(5, 0, 5, 0);
    c.fill = c.HORIZONTAL;
    c.anchor = c.CENTER;
    c.gridx = 0;
    c.gridy = 0;
    c.weightx = 1;
    c.weighty = 0;
    panouClienti.add(infoClienti, c);

    c.insets = new Insets(0, 0, 0, 0);
    c.fill = c.BOTH;
    c.gridy = 1;
    c.weighty = 1;
    panouClienti.add(scrollListaClienti, c);

    // un split pane care separa mesajele de pe server si panoul cu lista de 
    // clienti.
    tabServer = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, null, panouClienti);
    tabServer.setOneTouchExpandable(true);
    tabServer.setResizeWeight(0.85);
    tabServer.setName("#server");

    JLabel tabServerLabel = new JLabel("Server");
    tabServerLabel.setOpaque(false);

    // taburile de chat: initial doar cu server. mai tarziu pot fi adaugate mai 
    // multe taburi, pentru discutiile private cu utilizatorii conectati.
    chatTabs = new JTabbedPane();
    chatTabs.addTab("#server", tabServer);
    chatTabs.setSelectedIndex(0);
    chatTabs.setTabComponentAt(0, tabServerLabel);
    chatTabs.addChangeListener(this);

    // adaugam optiunile in interfata
    c.insets = new Insets(12, 12, 0, 12);

    c.fill = c.HORIZONTAL;
    c.anchor = c.WEST;
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;
    c.weightx = 1;
    c.weighty = 0;
    this.add(infoConexiune, c);

    c.fill = c.NONE;
    c.anchor = c.EAST;
    c.gridx = 2;
    c.gridwidth = 1;
    c.weightx = 0;
    this.add(btnDeconectare, c);

    c.insets = new Insets(12, 12, 5, 12);

    c.fill = c.BOTH;
    c.anchor = c.CENTER;
    c.gridx = 0;
    c.gridy = 2;
    c.gridwidth = c.REMAINDER;
    c.gridheight = c.RELATIVE;
    c.weightx = 1;
    c.weighty = 1;
    this.add(chatTabs, c);

    // adaugam in interfata optiunile ce tin de schimbarea numelui si de 
    // trimiterea de mesaje.
    JPanel userPanel = new JPanel(new GridBagLayout());

    c.insets = new Insets(0, 0, 0, 0);
    c.fill = c.NONE;
    c.anchor = c.WEST;
    c.gridx = 0;
    c.gridy = 1;
    c.gridwidth = 1;
    c.gridheight = 1;
    c.weightx = 0;
    c.weighty = 0;
    userPanel.add(btnNume, c);

    c.fill = c.HORIZONTAL;
    c.anchor = c.CENTER;
    c.gridx = 1;
    c.gridwidth = c.RELATIVE;
    c.weightx = 1;
    userPanel.add(msgField, c);

    c.fill = c.NONE;
    c.anchor = c.EAST;
    c.gridx = 2;
    c.gridwidth = 1;
    c.weightx = 0;
    userPanel.add(btnTrimite, c);

    c.insets = new Insets(0, 12, 12, 12);
    c.fill = c.HORIZONTAL;
    c.anchor = c.CENTER;
    c.gridx = 0;
    c.gridy = 3;
    c.gridwidth = c.REMAINDER;
    c.gridheight = 1;
    c.weightx = 1;
    c.weighty = 0;
    this.add(userPanel, c);

    this.setMinimumSize(new Dimension(100, 100));
  }

  // fereastra ce permite utilizatorului sa-si schimbe numele
  private void afisareSchimbareNume () {
    String numeNou = (String) JOptionPane.showInputDialog(myApp.getMyFrame(), "Nume nou:", "Schimbare nume - ChatApp", JOptionPane.PLAIN_MESSAGE, null, null, myApp.getNumeUtilizator());
    if (numeNou == null || numeNou.isEmpty() || numeNou.equals(myApp.getNumeUtilizator())) {
      return;
    }

    numeNou = numeNou.trim();
    if (!numeNou.isEmpty()) {
      numeNou = numeNou.replaceAll("/\\s+/", "-");
    }

    DefaultListModel model = (DefaultListModel) listaClienti.getModel();

    if (numeNou.equals("#server") || numeNou.equals(myApp.getNumeUtilizator()) || model.contains(numeNou)) {
      JOptionPane.showMessageDialog(myApp.getMyFrame(), "Noul nume este invalid.", "Eroare schimbare nume - ChatApp", JOptionPane.ERROR_MESSAGE);
      if (!msgField.isFocusOwner()) {
        msgField.requestFocusInWindow();
      }
      return;
    }

    if (!myApp.schimbareNume(numeNou)) {
      JOptionPane.showMessageDialog(myApp.getMyFrame(), "Numele tau nu a putut fi schimbat.", "Eroare schimbare nume - ChatApp", JOptionPane.ERROR_MESSAGE);
    }

    if (!msgField.isFocusOwner()) {
      msgField.requestFocusInWindow();
    }
  }

  // initializarea panoului chat, atunci cand utilizatorul se conecteaza la 
  // server. aceasta metoda actualizeaza elementele interfetei in functie de 
  // situatia curenta a conexiunii.
  public void initializare () {
    JFrame f = myApp.getMyFrame();

    if (f.getContentPane() == ((Container) this)) {
      return;
    }

    listaClienti.clearSelection();
    listaClienti.setModel(myApp.getListaClienti());
    listaClienti.ensureIndexIsVisible(0);
    msgField.setText("");

    chatTabs.setSelectedIndex(0);
    while (chatTabs.getTabCount() > 1) {
      chatTabs.remove(1);
    }

    // afisarea de mesaje trimise de catre server.
    mesajeServer = null;
    mesajeServer = new PanouMesaje();
    tabServer.setLeftComponent((JScrollPane) mesajeServer);

    actualizareStareConexiune();

    f.setContentPane((Container) this);
    f.setMinimumSize(new Dimension(300, 300));
    f.setPreferredSize(new Dimension(550, 450));
    f.pack();
    f.setLocationRelativeTo(null);

    tabServer.setDividerLocation(0.75);

    myApp.getRootPane().setDefaultButton(null);

    if (!msgField.isFocusOwner()) {
      msgField.requestFocusInWindow();
    }
  }

  // seteaza/actualizeaza titlul JFrameului aplicatiei si alte texte din Pagina 
  // Chat.
  private void actualizareStareConexiune () {
    btnNume.setText(myApp.getNumeUtilizator() + ":");

    if (myApp.getServerConectat()) {
      infoConexiune.setText("Conectat la " + myApp.getServerAdresa() + ":" + myApp.getServerPort() + ".");
      btnDeconectare.setText("Deconectare");
      btnNume.setEnabled(true);
    } else {
      infoConexiune.setText("Deconectat de la " + myApp.getServerAdresa() + ":" + myApp.getServerPort() + ".");
      btnDeconectare.setText("Iesire");
      btnNume.setEnabled(false);
    }

    int nr = listaClienti.getModel().getSize();

    if (nr == 0) {
      infoClienti.setText("Nici un client");
    } else if (nr == 1) {
      infoClienti.setText("Un client");
    } else {
      infoClienti.setText(nr + " clienti");
    }

    myApp.getMyFrame().setTitle("ChatApp - " + myApp.getNumeUtilizator() + " - " + myApp.getServerAdresa() + ":" + myApp.getServerPort());

    actualizareTabActiv();
  }

  // event handler pentru butoanele din panou: schimbare nume, deconectare si 
  // trimitere mesaj.
  public void actionPerformed (ActionEvent ev) {
    Object src = ev.getSource();
    String cmd = ev.getActionCommand();

    if (cmd == null) {
      System.err.println("PaginaChat.actionPerformed: cmd == null");
      return;

    } else if (cmd.equals("trimiteMesaj")) { 
      trimiteMesaj();

    } else if (cmd.equals("schimbareNume")) { 
      afisareSchimbareNume();

    } else if (cmd.equals("inchidereTab") && src != null && src instanceof JButton) {
      JButton btn = (JButton) src;
      String nume = btn.getName();
      if (nume == null) {
        return;
      }

      int t = chatTabs.indexOfTab(nume);
      if (t == -1) {
        return;
      }
      chatTabs.remove(t);

      if (!msgField.isFocusOwner()) {
        msgField.requestFocusInWindow();
      }

    } else if (cmd.equals("deconectare")) {
      myApp.deconectareGUI();
    }
  }

  // event handler pentru schimbarea de taburi.
  public void stateChanged (ChangeEvent ev) {
    Object src = ev.getSource();
    if (!(src instanceof JTabbedPane)) {
      return;
    }

    actualizareTabActiv();

    if (msgField.isEnabled() && !msgField.isFocusOwner()) {
      msgField.requestFocusInWindow();
    }

    int t = chatTabs.getSelectedIndex();
    if (t != -1) {
      String tab = chatTabs.getTitleAt(t);

      Component compot = chatTabs.getTabComponentAt(t);
      if (compot == null) {
        return;
      } else if (tab.equals("#server") && compot instanceof JLabel) {
        ((JLabel)compot).setForeground(Color.BLACK);
      } else if (!tab.equals("#server") && compot instanceof JPanel) {
        Component eticheta = ((JPanel)compot).getComponent(0);
        if (eticheta != null && eticheta instanceof JLabel) {
          ((JLabel)eticheta).setForeground(Color.BLACK);
        }
      }
    }
  }

  // event handler pentru clickul in lista de utilizatori.
  private MouseListener listaClientiMouseListener = new MouseAdapter () {
    public void mouseClicked (MouseEvent ev) {
      if (ev.getClickCount() != 2) {
        return;
      }

      int c = listaClienti.locationToIndex(ev.getPoint());
      if (c != -1) {
        activareChatPrivat(c);
      }
    }
  };

  // event handler pentru tastare in listaClienti.
  private KeyListener fieldKeyListener = new KeyAdapter () {
    public void keyPressed (KeyEvent ev) {
      if (ev.getKeyCode() == KeyEvent.VK_ENTER && ev.getSource() == listaClienti) {
        int i = listaClienti.getSelectedIndex();
        if (i != -1) {
          activareChatPrivat(i);
        }
      }
    }
  };

  // in functie de tabul activ, aceasta metoda activeaza/dezactiveaza campul de 
  // text si butonul Trimite.
  private void actualizareTabActiv () {
    boolean allowSend = true;

    Component c = chatTabs.getSelectedComponent();
    if (c != null) {
      String nume = c.getName();
      DefaultListModel model = (DefaultListModel) listaClienti.getModel();
      if (nume == null || (!nume.equals("#server") && !model.contains(nume))) {
        allowSend = false;
      }
    } else {
      allowSend = false;
    }

    if (myApp.getServerConectat() && allowSend) {
      if (!btnTrimite.isEnabled()) {
        btnTrimite.setEnabled(true);
      }
      if (!msgField.isEnabled()) {
        msgField.setEnabled(true);
      }
      if (!msgField.isEditable()) {
        msgField.setEditable(true);
      }
    } else {
      if (btnTrimite.isEnabled()) {
        btnTrimite.setEnabled(false);
      }
      if (msgField.isEnabled()) {
        msgField.setEnabled(false);
      }
      if (msgField.isEditable()) {
        msgField.setEditable(false);
      }
    }
  }

  // metoda permite initierea unui chat privat cu un utilizator, pe baza 
  // indexului in listaClienti.
  private void activareChatPrivat (int c) {
    if (c < 0) {
      return;
    }

    DefaultListModel model = (DefaultListModel) listaClienti.getModel();
    String nume = (String) model.getElementAt(c);
    if (nume == null || nume.equals(myApp.getNumeUtilizator())) {
      return;
    }

    int t = chatTabs.indexOfTab(nume);
    if (t == -1) {
      t = adaugaChatPrivat(nume);
    }

    if (t != -1) {
      chatTabs.setSelectedIndex(t);
      if (msgField.isEnabled() && !msgField.isFocusOwner()) {
        msgField.requestFocusInWindow();
      }
    }
  }

  // aceasta metoda adauga un tab nou ce poate fi folosit pentru chat privat cu 
  // un utilizator.
  private int adaugaChatPrivat (String nume) {
    int i = chatTabs.indexOfTab(nume);
    if (i != -1 || nume == null || nume.equals("#server") || nume.equals(myApp.getNumeUtilizator())) {
      return -1;
    }

    JScrollPane mesaje = new PanouMesaje();
    mesaje.setName(nume);

    JLabel eticheta = new JLabel(nume);
    eticheta.setOpaque(false);

    JButton btn = new JButton("X");
    btn.setUI(new BasicButtonUI());
    btn.setContentAreaFilled(false);
    btn.setOpaque(false);
    btn.setMargin(new Insets(0,0,0,0));
    btn.setBorder(new EmptyBorder(new Insets(0,5,0,0)));
    btn.setToolTipText("Inchide discutia");
    btn.setName(nume);
    btn.setActionCommand("inchidereTab");
    btn.addActionListener(this);

    JPanel panou = new JPanel(new BorderLayout());
    panou.setOpaque(false);
    panou.add(eticheta, BorderLayout.CENTER);
    panou.add(btn, BorderLayout.EAST);

    chatTabs.addTab(nume, mesaje);

    i = chatTabs.indexOfTab(nume);
    if (i != -1) {
      chatTabs.setTabComponentAt(i, panou);
    } else {
      System.err.println("PaginaChat.adaugaChatPrivat('" + nume + "') eroare: indexOfTab(nume) == -1");
    }

    return i;
  }

  // metoda permite adaugarea de mesaje in oricare tab
  private void adaugaMesajInTab (String numeTab, String msg, String cssClass) {
    int t = chatTabs.indexOfTab(numeTab);
    int ts = chatTabs.getSelectedIndex();

    //myApp.mesajDebug("PaginaChat.adaugaMesajInTab " + numeTab + " t " + t + " ts " + ts + " class " + cssClass + " msg " + msg);

    if (numeTab.equals("#server")) {
      mesajeServer.adaugaMesaj(msg, cssClass);
      if (t == -1) {
        return;
      }

      if (ts != t) {
        Component compot = chatTabs.getTabComponentAt(t);
        if (compot == null || !(compot instanceof JLabel)) {
          return;
        }
        ((JLabel)compot).setForeground(Color.BLUE);
      }
    } else {
      if (t == -1) {
        if (cssClass.equals("mesaj")) {
          t = adaugaChatPrivat(numeTab);
          if (t == -1) {
            return;
          }
        } else {
          return;
        }
      }

      Component compom = chatTabs.getComponentAt(t);
      if (compom == null || !(compom instanceof PanouMesaje)) {
        return;
      }

      PanouMesaje panoum = (PanouMesaje) compom;
      panoum.adaugaMesaj(msg, cssClass);

      if (ts != t) {
        Component compot = chatTabs.getTabComponentAt(t);
        if (compot == null || !(compot instanceof JPanel)) {
          return;
        }
        Component compoe = ((JPanel) compot).getComponent(0);
        if (compoe != null && compoe instanceof JLabel) {
          ((JLabel)compoe).setForeground(Color.BLUE);
        }
      }
    }
  }

  // metoda este chemata cand utilizatorul trimite un mesaj.
  private void trimiteMesaj () {
    String msg = msgField.getText();
    if (msg.trim().isEmpty()) {
      return;
    }

    DefaultListModel model = (DefaultListModel) listaClienti.getModel();
    String catre = chatTabs.getSelectedComponent().getName();

    if (catre == null || catre.equals(myApp.getNumeUtilizator())) {
      return;
    } else if (catre.equals("#server") || model.contains(catre)) {
      myApp.trimiteMesaj(catre, msg);
    }

    msgField.setText("");
    if (!msgField.isFocusOwner()) {
      msgField.requestFocusInWindow();
    }
  }

  public void ev_mesajTrimis (String catre, String msg) {
    if (catre == null || catre.trim().isEmpty() || msg == null || msg.trim().isEmpty()) {
      return;
    }

    String msgf = "<span class='nume'>" + htmlEscape(myApp.getNumeUtilizator()) + "</span>: " + htmlEscape(msg);

    adaugaMesajInTab(catre, msgf, "mesaj");
  }

  // urmeaza o serie de metode invocate atunci cand un mesaj este primit de la 
  // server.

  public void ev_conectareClient (String nume) {
    actualizareStareConexiune();

    String msg = "<span class='nume'>" + htmlEscape(nume) + "</span> s-a conectat la server.";

    adaugaMesajInTab("#server", msg, "conectat");
    adaugaMesajInTab(nume, msg, "conectat");
  }

  public void ev_deconectareClient (String nume) {
    actualizareStareConexiune();

    if (nume.equals("#server")) {
      adaugaMesajInTab(nume, "Serverul a inchis conexiunea.", "deconectat");
      return;
    }

    String msg = "<span class='nume'>" + htmlEscape(nume) + "</span> s-a deconectat de la server.";
    adaugaMesajInTab("#server", msg, "deconectat");
    adaugaMesajInTab(nume, msg, "deconectat");
  }

  public void ev_schimbareNume (String numeVechi, String numeNou) {
    if (numeNou.equals(myApp.getNumeUtilizator())) {
      btnNume.setText(numeNou + ":");
      myApp.getMyFrame().setTitle("ChatApp - " + myApp.getNumeUtilizator() + " - " + myApp.getServerAdresa() + ":" + myApp.getServerPort());
    }

    String msg = "<span class='nume'>" + htmlEscape(numeVechi) + "</span> si-a schimbat numele in <span class='nume'>" + htmlEscape(numeNou) + "</span>.";
    adaugaMesajInTab("#server", msg, "schimbareNume");
    adaugaMesajInTab(numeVechi, msg, "schimbareNume");

    int t = chatTabs.indexOfTab(numeVechi);
    int t2 = chatTabs.indexOfTab(numeNou);
    if (t2 != -1) {
      adaugaMesajInTab(numeNou, msg, "schimbareNume");
    }

    if (t != -1 && t2 == -1) {
      Component compom = chatTabs.getComponentAt(t);
      if (compom == null || !(compom instanceof PanouMesaje)) {
        return;
      }

      ((PanouMesaje) compom).setName(numeNou);
      chatTabs.setTitleAt(t, numeNou);

      Component compot = chatTabs.getTabComponentAt(t);
      if (compot == null || !(compot instanceof JPanel)) {
        return;
      }

      Component eticheta = ((JPanel) compot).getComponent(0);
      if (eticheta != null && eticheta instanceof JLabel) {
        ((JLabel) eticheta).setText(numeNou);
      }

      Component btn = ((JPanel) compot).getComponent(1);
      if (btn != null && btn instanceof JButton) {
        ((JButton) btn).setName(numeNou);
      }
    }

    actualizareTabActiv();
  }

  public void ev_mesajPrimit (String dela, String catre, String msg) {
    if (dela.equals("#server") || dela.equals(myApp.getNumeUtilizator())) {
      //myApp.mesajDebug("PaginaChat.ev_mesajPrimit dela = " + dela);
      return;
    }

    //myApp.mesajDebug("PaginaChat.ev_mesajPrimit dela " + dela + " catre " + catre + " msg " + msg);

    String msgf = "<span class='nume'>" + htmlEscape(dela) + "</span>: " + htmlEscape(msg);

    if (catre.equals("#server")) {
      adaugaMesajInTab(catre, msgf, "mesaj");
    } else if (catre.equals(myApp.getNumeUtilizator())) {
      adaugaMesajInTab(dela, msgf, "mesaj");
    }
  }

  public void ev_listaClienti () {
    actualizareStareConexiune();
  }

  private String htmlEscape (String htmlStr) {
    return htmlStr.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
  }
}
