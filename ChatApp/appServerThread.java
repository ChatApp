/*
 * © 2010 ROBO Design
 * http://www.robodesign.ro
 *
 * $Date: 2010-05-19 18:44:30 +0300 $
 */

package ChatApp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

// clasa de tip thread ce gestioneaza o conexiune cu un client. pentru fiecare 
// client un obiect nou este alocat si executat intr-un thread separat.

public class appServerThread extends Thread {
  private Socket client = null;
  private appServer server;
  private PrintStream out = null;
  private BufferedReader in = null;
  private volatile String numeUtilizator = null;
  private volatile boolean clientInitializat = false;
  private volatile boolean eroare = false;

  // initializare thread
  public appServerThread (String nume, Socket sock, appServer srv) {
    server = srv;
    client = sock;
    numeUtilizator = nume;
    server.myApp.mesajDebug("appServerThread initializare " + nume);

    if (!server.getServerPornit()) {
      System.err.println("appServerThread: serverul nu este pornit");
      eroare = true;
      return;
    }

    try {
      out = new PrintStream(client.getOutputStream(), true, "utf-8");
      in = new BufferedReader(new InputStreamReader(client.getInputStream(), "utf-8"));
      server.myApp.mesajDebug("appServerThread initializare reusita " + numeUtilizator);

    } catch (Exception ex) {
      System.err.println("appServerThread: esec initializare. " + ex);
      ex.printStackTrace();

      try {
        if (out != null) {
          out.close();
          out = null;
        }
        if (!client.isClosed()) {
          client.close();
        }
      } catch (Exception ex2) {
        System.err.println("appServerThread: esec total. " + ex2);
        ex2.printStackTrace();
      }

      client = null;
      eroare = true;
    }
  }

  // metoda care ruleaza in thread separat
  public void run () {
    if (!server.getServerPornit() || client == null || client.isClosed() || out == null || out.checkError() || in == null) {
      System.err.println("appServerThread.run esec " + numeUtilizator);
      oprire();
      return;
    }

    server.myApp.mesajDebug("appServerThread.run " + numeUtilizator);

    // primire comenzi de la client
    String cmd = null;
    while (server.getServerPornit() && in != null && client != null && !client.isClosed()) {
      try {
        cmd = in.readLine();
      } catch (Exception ex) {
        System.err.println("appServerThread.run readLine() a esuat. " + ex);
        ex.printStackTrace();
        break;
      }

      if (cmd == null || cmd.trim().isEmpty()) {
        break;
      }

      server.procesareComanda(cmd, this);
      cmd = null;
    }

    server.myApp.mesajDebug("appServerThread.run end " + numeUtilizator);

    oprire();
  }

  // metoda ce permite trimiterea de mesaje la client
  public synchronized boolean trimiteComanda (String cmd) {
    if (out == null || out.checkError() || !server.getServerPornit() || client == null || client.isClosed() || eroare || cmd == null || cmd.trim().isEmpty()) {
      System.err.println("appServerThread.trimiteComanda " + numeUtilizator + " esec. Starea conexiunii este invalida.");
      return false;
    }

    server.myApp.mesajDebug("appServerThread.trimiteComanda " + numeUtilizator + " " + cmd);

    try {
      out.println(cmd);
    } catch (Exception ex) {
      System.err.println("appServerThread.trimiteComanda " + numeUtilizator + " a esuat. " + ex);
      ex.printStackTrace();
      return false;
    }

    return !out.checkError();
  }

  public String getNumeUtilizator () {
    return numeUtilizator;
  }

  public synchronized boolean setNumeUtilizator (String nume) {
    if (nume == null || nume.isEmpty() || nume.trim().isEmpty() || nume.indexOf(" ") != -1 || nume.equals("#server")) {
      System.err.println("appServerThread.setNumeUtilizator nume invalid: " + nume);
      return false;
    } else {
      numeUtilizator = nume;
      return true;
    }
  }

  public boolean getClientInitializat () {
    return clientInitializat;
  }

  public synchronized boolean setClientInitializat (boolean val) {
    if (numeUtilizator != null && !eroare && client != null && !client.isClosed()) {
      clientInitializat = val;
      return true;
    } else {
      return false;
    }
  }

  public boolean getEroare () {
    if (!eroare && (numeUtilizator == null || out == null || in == null || client == null || client.isClosed())) {
      eroare = true;
    }

    return eroare;
  }

  // oprire conexiune cu client
  public void oprire () {
    server.myApp.mesajDebug("appServerThread.oprire " + numeUtilizator);

    try {
      if (in != null) {
        in.close();
        in = null;
      }

      if (out != null) {
        out.close();
        out = null;
      }

      if (client != null && !client.isClosed()) {
        client.close();
      }
      client = null;
    } catch (Exception ex) {
      System.err.println("appServerThread.oprire cu erori. " + ex);
      ex.printStackTrace();
    }

    server.clientIesire(this);
    clientInitializat = false;
  }
}

// vim:set fo=wancroql tw=80 ts=2 sw=2 sts=2 sta et ai cin ff=unix:
