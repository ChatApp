/*
 * © 2010 ROBO Design
 * http://www.robodesign.ro
 *
 * $Date: 2010-05-19 18:42:42 +0300 $
 */

package ChatApp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import javax.swing.DefaultListModel;
import javax.swing.SwingUtilities;

// clasa ce se ocupa de conectarea unui client la un server de chat.

public class appClient extends Thread {
  private appMain myApp;
  private volatile String serverAdresa;
  private volatile int serverPort;
  private volatile String numeUtilizator;
  private volatile boolean serverConectat;
  private Socket clientSocket;
  private PrintStream clientOut;
  private BufferedReader clientIn;
  private DefaultListModel listaClienti;

  // initializare clasa
  public appClient (appMain rootApp) {
    myApp = rootApp;
    listaClienti = new DefaultListModel();

    serverAdresa = "";
    serverPort = 0;
    numeUtilizator = "";
    serverConectat = false;

    clientSocket = null;
    clientOut = null;
    clientIn = null;
  }

  // conectare la server
  public boolean conectare (String nume, String server, int port) {
    if (serverConectat) {
      if (server.equals(serverAdresa) && port == serverPort && nume.equals(numeUtilizator)) {
        return true;
      } else {
        System.err.println("appClient.conectare: aplicatia este deja conectata la un alt server.");
        return false;
      }
    } else if (clientSocket != null) {
      System.err.println("appClient.conectare: aplicatia este intr-o stare invalida.");
      return false;
    }

    // verificare nume
    if (nume == null || nume.isEmpty() || nume.trim().isEmpty() || nume.indexOf(" ") != -1 || nume.equals("#server") || listaClienti.contains(nume)) {
      System.err.println("appClient.conectare: numele dat este invalid. " + nume);
      return false;
    }

    serverAdresa = server;
    serverPort = port;
    numeUtilizator = nume;

    myApp.mesajDebug("appClient.conectare " + numeUtilizator + " " + serverAdresa + " " + serverPort);

    // pornire conexiune socket
    try {
      clientSocket = new Socket(serverAdresa, serverPort);
      clientOut = new PrintStream(clientSocket.getOutputStream(), true, "utf-8");
      clientIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), "utf-8"));

    } catch (UnknownHostException ex) {
      System.err.println("appClient.conectare: Serverul nu poate fi gasit: " + serverAdresa + ". " + ex);
      ex.printStackTrace();

      clientSocket = null;
      clientOut = null;
      clientIn = null;

      return false;

    } catch (Exception ex) {
      System.err.println("appClient.conectare: Conectarea la server a esuat. " + ex);
      ex.printStackTrace();

      clientSocket = null;
      clientOut = null;
      clientIn = null;

      return false;
    }

    serverConectat = true;

    // pornire thread separat de monitorizare a socketului
    start();

    return true;
  }

  // metoda ce ruleaza constant intr-un thread separat, cat timp utilizatorul 
  // este conectat la server.
  public void run () {
    if (!serverConectat || clientIn == null || clientSocket == null || clientSocket.isClosed()) {
      System.err.println("appClient.run: starea aplicatiei este invalida.");
      serverConectat = false;
      return;
    }

    myApp.mesajDebug("appClient.run start " + numeUtilizator);

    try {
      SwingUtilities.invokeAndWait(new Runnable () {
        public void run () {
          myApp.afisarePaginaChat();
        }
      });
    } catch (Exception ex) {
      System.err.println("appClient.run: afisarePaginaChat() a esuat. " + ex);
      ex.printStackTrace();
      serverConectat = false;
    }

    if (!trimiteComanda("/conectare " + numeUtilizator)) {
      System.err.println("appClient.run: trimiteComanda('/conectare') a esuat. ");
      serverConectat = false;
    }

    String cmd = null;

    while (serverConectat && clientIn != null && clientSocket != null && !clientSocket.isClosed()) {
      try {
        cmd = clientIn.readLine();
      } catch (Exception ex) {
        System.err.println("appClient.run: clientIn.readLine a esuat. " + ex);
        ex.printStackTrace();
        break;
      }

      if (cmd == null || cmd.trim().isEmpty()) {
        break;
      }

      procesareComanda(cmd);
      cmd = null;
    }

    myApp.mesajDebug("appClient.run end " + numeUtilizator);

    serverConectat = false;

    try {
      if (clientOut != null) {
        clientOut.close();
      }
      if (clientIn != null) {
        clientIn.close();
      }
      if (clientSocket != null && !clientSocket.isClosed()) {
        clientSocket.close();
      }
    } catch (Exception ex) {
      System.err.println("appClient.deconectare: inchidere socket esuata. " + ex);
      ex.printStackTrace();
    }

    clientSocket = null;
    clientOut = null;
    clientIn = null;

    if (!listaClienti.isEmpty()) {
      listaClienti.clear();
    }

    SwingUtilities.invokeLater(new Runnable () {
      public void run () {
        myApp.deconectareClientCompleta();
      }
    });
  }

  // metoda ce permite trimiterea de comenzi de la client catre server.
  private synchronized boolean trimiteComanda (String cmd) {
    if (!serverConectat || clientSocket == null || clientSocket.isClosed() || clientOut == null || clientOut.checkError() || cmd == null || cmd.isEmpty()) {
      System.err.println("appClient.trimiteComanda a esuat. Serverul nu este conectat, sau starea aplicatiei este invalida.");
      return false;
    }

    myApp.mesajDebug("appClient.trimiteComanda " + cmd);

    try {
      clientOut.println(cmd);
    } catch (Exception ex) {
      System.err.println("appClient.trimiteComanda a esuat. " + ex);
      ex.printStackTrace();
      return false;
    }

    return !clientOut.checkError();
  }

  // metoda ce proceseaza comenzile primite de la server.
  private boolean procesareComanda (String cmd) {
    if (!serverConectat) {
      System.err.println("appClient.procesareComanda: serverul nu este conectat.");
      return false;
    }

    String[] arg = cmd.split(" ");
    String fn = arg[0]; // functia, primul element

    myApp.mesajDebug("appClient.procesareComanda " + fn + " cmd " + cmd);

    if ("/conectare".equals(fn) && arg.length == 2) {
      ev_conectareClient(arg[1]);
    } else if ("/deconectare".equals(fn) && arg.length == 2) {
      ev_deconectareClient(arg[1]);
    } else if ("/conflict-nume".equals(fn) && arg.length == 1) {
      ev_conflictNume();
    } else if ("/mesaj".equals(fn) && arg.length > 2) {
      ev_mesajPrimit(arg[1], arg[2], myApp.stringArrayJoin(arg, " ", 3));
    } else if ("/nume".equals(fn) && arg.length == 3) {
      ev_schimbareNume(arg[1], arg[2]);
    } else if ("/lista".equals(fn) && arg.length > 1) {
      ev_listaClienti(arg);
    } else {
      return false;
    }

    return true;
  }

  // metoda permite trimiterea de mesaje catre alti utilizatori, de la 
  // utilizatorul/clientul actual.
  public boolean trimiteMesaj (String catre, String msg) {
    if (!serverConectat || catre == null || catre.trim().isEmpty() || catre.indexOf(" ") != -1 || msg == null || msg.trim().isEmpty() || catre.equals(numeUtilizator) || (!catre.equals("#server") && !listaClienti.contains(catre))) {
      System.err.println("appClient.trimiteMesaj: parametrii nu sunt validati.");
      return false;
    }

    return trimiteComanda("/mesaj " + numeUtilizator + " " + catre + " " + msg);
  }

  // metoda permite clientului sa-si schimbe numele.
  public boolean schimbareNume (String numeNou) {
    if (!serverConectat || numeNou == null || numeNou.trim().isEmpty() || numeNou.indexOf(" ") != -1 || numeNou.equals("#server") || listaClienti.contains(numeNou)) {
      System.err.println("appClient.schimbareNume: parametrii nu sunt validati.");
      return false;
    }

    return trimiteComanda("/nume " + numeUtilizator + " " + numeNou);
  }

  // urmeaza cateva metode care sunt chemate in funcite de comenzile primite de 
  // la server.

  private synchronized void ev_conectareClient (String nume) {
    myApp.mesajDebug("appClient.ev_conectareClient " + nume);

    if (nume.equals("#server")) {
      System.err.println("appClient.ev_conectareClient numele este invalid: " + nume);
      return;
    }

    if (!listaClienti.contains(nume)) {
      listaClienti.addElement(nume);
      myApp.ev_conectareClient(nume);
    } else {
      System.err.println("appClient.ev_conectareClient numele exista deja " + nume);
    }
  }

  private synchronized void ev_deconectareClient (String nume) {
    myApp.mesajDebug("appClient.ev_deconectareClient " + nume);

    if (nume.equals("#server") || nume.equals(numeUtilizator)) {
      deconectare();
      myApp.ev_deconectareClient(nume);
    } else if (listaClienti.contains(nume)) {
      listaClienti.removeElement(nume);
      myApp.ev_deconectareClient(nume);
    } else {
      System.err.println("appClient.ev_deconectareClient numele nu exista " + nume);
    }
  }

  private void ev_conflictNume () {
    String numeNou = myApp.ev_conflictNume();
    if (numeNou == null || numeNou.trim().isEmpty() || numeNou.indexOf(" ") != -1 || numeNou.equals("#server")) {
      myApp.deconectareGUI();
      return;
    }

    numeUtilizator = numeNou;
    trimiteComanda("/conectare " + numeNou);
  }

  private synchronized void ev_schimbareNume (String numeVechi, String numeNou) {
    if (numeVechi.equals("#server") || numeNou.equals("#server")) {
      return;
    }

    int i = listaClienti.indexOf(numeVechi);
    if (i != -1) {
      listaClienti.set(i, numeNou);
    } else {
      System.err.println("appClient.ev_schimbareNume numeVechi nu exista: " + numeVechi);
      listaClienti.addElement(numeNou);
    }

    // schimbarea numelui, la clientul local
    if (numeVechi.equals(numeUtilizator)) {
      numeUtilizator = numeNou;
    }

    myApp.ev_schimbareNume(numeVechi, numeNou);
  }

  private synchronized void ev_listaClienti (String[] raw) {
    myApp.mesajDebug("appClient.ev_listaClienti");

    if (!raw[0].equals("/lista")) {
      System.err.println("appClient.ev_listaClienti raw[0] != /lista");
      return;
    }

    listaClienti.clear();
    for (int i = 1; i < raw.length; i++) {
      listaClienti.addElement(raw[i]);
    }

    myApp.ev_listaClienti();
  }

  private void ev_mesajPrimit (String dela, String catre, String msg) {
    myApp.mesajDebug("appClient.ev_mesajPrimit dela " + dela + " catre " + catre);

    if (dela.equals(catre) || dela.equals("#server")) {
      System.err.println("appClient.ev_mesajPrimit eronat dela " + dela + " catre " + catre);
    } else if (catre.equals("#server") || dela.equals(numeUtilizator) || catre.equals(numeUtilizator)) {
      myApp.ev_mesajPrimit(dela, catre, msg);
    } else {
      System.err.println("appClient.ev_mesajPrimit eronat dela " + dela + " catre " + catre);
    }
  }

  // metoda permite deconectarea de la server
  public void deconectare () {
    if (!serverConectat) {
      return;
    }
    myApp.mesajDebug("appClient.deconectare " + numeUtilizator);

    trimiteComanda("/deconectare " + numeUtilizator);

    serverConectat = false;
    if (!listaClienti.isEmpty()) {
      listaClienti.clear();
    }
  }

  public boolean getServerConectat () {
    return serverConectat;
  }

  public String getServerAdresa () {
    return serverAdresa;
  }

  public int getServerPort () {
    return serverPort;
  }

  public String getNumeUtilizator () {
    return numeUtilizator;
  }

  public DefaultListModel getListaClienti () {
    return listaClienti;
  }
}

// vim:set fo=wancroql tw=80 ts=2 sw=2 sts=2 sta et ai cin ff=unix:

