/*
 * © 2010 ROBO Design
 * http://www.robodesign.ro
 *
 * $Date: 2010-05-19 18:35:24 +0300 $
 */

package ChatApp;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

// Panoul principal, "prima pagina". Acesta permite utilizatorului:
// - sa porneasca serverul pentru a gazdui un chat,
// - sa se conecteze la un alt server, la un chat existent.

public class PrimaPagina extends JPanel implements ActionListener, ChangeListener {
  private appMain myApp;
  private JTabbedPane panouTaburi;
  private JSpinner serverPortField;
  private JSpinner clientPortField;
  private final int portMin = 1024;
  private final int portMax = 65535;
  private JTextField serverNickField;
  private JTextField clientNickField;
  private JTextField serverAddressField;
  private JButton btnConectareServer;
  private JButton btnStartServer;

  public PrimaPagina (appMain rootApp) {
    super(new GridBagLayout());

    GridBagConstraints c = new GridBagConstraints();
    myApp = rootApp;

    // taburile principale ale aplicatiei.
    panouTaburi = new JTabbedPane();
    panouTaburi.setName("taburiPrimaPagina");
    panouTaburi.addTab("Conectare la server", panouConectareServer());
    panouTaburi.addTab("Gazduire server", panouGazduireChat());
    panouTaburi.addChangeListener(this);

    c.fill = c.BOTH;
    c.anchor = c.CENTER;
    c.gridx = 0;
    c.gridy = 0;
    c.weightx = 1;
    c.weighty = 1;
    c.insets = new Insets(12, 12, 6, 12);
    this.add(panouTaburi, c);

    JButton btnIesire = new JButton("Iesire");
    btnIesire.setToolTipText("Iesire din aplicatie.");
    btnIesire.setActionCommand("iesireApp");
    btnIesire.setDefaultCapable(false);
    btnIesire.addActionListener(this);

    c.fill = c.NONE;
    c.anchor = c.EAST;
    c.gridy = 1;
    c.gridheight = c.RELATIVE;
    c.weighty = 0;
    c.insets = new Insets(6, 12, 12, 12);
    this.add(btnIesire, c);
  }

  // initializarea interfetei
  public void initializare () {
    JFrame f = myApp.getMyFrame();

    if (f.getContentPane() == ((Container) this)) {
      return;
    }

    f.setContentPane((Container) this);
    f.setTitle("ChatApp");
    f.setSize(new Dimension(360, 310));
    f.setMinimumSize(new Dimension(360, 310));
    f.setPreferredSize(new Dimension(360, 310));
    f.pack();
    f.setLocationRelativeTo(null);

    Component tab = panouTaburi.getSelectedComponent();

    if (tab.getName().equals("conectareServer")) {
      myApp.getRootPane().setDefaultButton(btnConectareServer);
      clientNickField.requestFocusInWindow();
    } else if (tab.getName().equals("gazduireChat")) {
      myApp.getRootPane().setDefaultButton(btnStartServer);
      serverNickField.requestFocusInWindow();
    } else {
      myApp.getRootPane().setDefaultButton(null);
    }
  }

  // event handler pentru butoanele din panou: gazduire server si conectare la 
  // server.
  public void actionPerformed (ActionEvent ev) {
    String cmd = ev.getActionCommand();

    String nick = "";

    if (cmd.equals("startServer")) {
      nick = serverNickField.getText();
    } else if (cmd.equals("conectareServer")) {
      nick = clientNickField.getText();
    }

    // validare nume
    if (!nick.isEmpty()) {
      nick = nick.trim();
      if (!nick.isEmpty()) {
        nick = nick.replaceAll("/\\s+/", "-");
      }
      if (nick.equals("#server")) {
        nick = "";
      }
    }

    if (cmd.equals("startServer") && !nick.isEmpty()) {
      int port = ((SpinnerNumberModel) serverPortField.getModel()).getNumber().intValue();
      myApp.startServer(nick, port);

    } else if (cmd.equals("conectareServer") && !nick.isEmpty() &&
        !serverAddressField.getText().trim().isEmpty()) {

      int port = ((SpinnerNumberModel) clientPortField.getModel()).getNumber().intValue();
      myApp.conectareServer(nick, serverAddressField.getText().trim(), port);

    } else if (cmd.equals("iesireApp")) {
      myApp.iesireApp();
    }
  }

  // event handler pentru schimbarea tabului activ.
  public void stateChanged (ChangeEvent ev) {
    Object src = ev.getSource();
    if (!(src instanceof JTabbedPane)) {
      return;
    }

    JTabbedPane panou = (JTabbedPane) src;
    if (panou == panouTaburi) {
      int sel = panou.getSelectedIndex();
      if (sel == 0) {
        myApp.getRootPane().setDefaultButton(btnConectareServer);
      } else if (sel == 1) {
        myApp.getRootPane().setDefaultButton(btnStartServer);
      } else {
        myApp.getRootPane().setDefaultButton(null);
      }
    }
  }

  // panoul ce permite gazduirea de server chat.
  private JPanel panouGazduireChat () {
    GridBagConstraints c = new GridBagConstraints();
    c.fill = c.HORIZONTAL;
    c.anchor = c.WEST;
    c.insets = new Insets(5, 5, 5, 5);

    JPanel panou = new JPanel(new GridBagLayout());
    panou.setName("gazduireChat");

    SpinnerNumberModel portModel = new SpinnerNumberModel(portMin, portMin, portMax, 1);
    serverPortField = new JSpinner(portModel);
    serverPortField.setToolTipText("Portul pe carte serverul asculta conexiuni de la clienti.");

    JLabel portLabel = new JLabel("Portul de ascultare:");
    portLabel.setLabelFor(serverPortField);

    serverNickField = new JTextField();
    serverNickField.setColumns(10);
    serverNickField.setToolTipText("Numele tau, in calitatea de client pe server, folosit in discutiile cu ceilalti clienti.");

    JLabel serverNickLabel = new JLabel("Numele tau:");
    serverNickLabel.setLabelFor(serverNickField);

    btnStartServer = new JButton("Porneste serverul!");
    btnStartServer.setDefaultCapable(true);
    btnStartServer.setActionCommand("startServer");
    btnStartServer.addActionListener(this);

    c.gridx = 0;
    c.gridy = 0;
    panou.add(serverNickLabel, c);

    c.gridy = 1;
    panou.add(portLabel, c);

    c.gridx = 1;
    c.gridy = 0;
    c.weightx = 1;
    c.gridwidth = c.RELATIVE;
    panou.add(serverNickField, c);

    c.gridy = 1;
    panou.add(serverPortField, c);

    c.gridx = 0;
    c.gridy = 2;
    c.anchor = c.NORTHEAST;
    c.fill = c.NONE;
    c.gridwidth = 2;
    c.gridheight = c.REMAINDER;
    c.weighty = 1;
    panou.add(btnStartServer, c);

    return panou;
  }

  // panoul ce permite conectarea la un server de chat.
  private JPanel panouConectareServer () {
    GridBagConstraints c = new GridBagConstraints();
    c.fill = c.HORIZONTAL;
    c.anchor = c.WEST;
    c.insets = new Insets(5, 5, 5, 5);

    JPanel panou = new JPanel(new GridBagLayout());
    panou.setName("conectareServer");

    clientNickField = new JTextField();
    clientNickField.setColumns(10);
    clientNickField.setToolTipText("Numele tau in discutiile cu ceilalti clienti conectati la server.");

    JLabel clientNickLabel = new JLabel("Numele tau:");
    clientNickLabel.setLabelFor(clientNickField);

    serverAddressField = new JTextField();
    serverAddressField.setColumns(10);

    JLabel serverAddressLabel = new JLabel("Adresa serverului:");
    serverAddressLabel.setLabelFor(serverAddressField);

    SpinnerNumberModel portModel = new SpinnerNumberModel(portMin, portMin, portMax, 1);
    clientPortField = new JSpinner(portModel);
    clientPortField.setToolTipText("Portul pe carte serverul asculta conexiuni de la clienti.");

    JLabel portLabel = new JLabel("Portul serverului:");
    portLabel.setLabelFor(clientPortField);

    btnConectareServer = new JButton("Conecteaza-te la server!");
    btnConectareServer.setDefaultCapable(true);
    btnConectareServer.setActionCommand("conectareServer");
    btnConectareServer.addActionListener(this);

    c.gridx = 0;
    c.gridy = 0;
    panou.add(clientNickLabel, c);

    c.gridy = 1;
    panou.add(serverAddressLabel, c);

    c.gridy = 2;
    panou.add(portLabel, c);

    c.gridx = 1;
    c.gridy = 0;
    c.weightx = 1;
    c.gridwidth = c.RELATIVE;
    panou.add(clientNickField, c);

    c.gridy = 1;
    panou.add(serverAddressField, c);

    c.gridy = 2;
    panou.add(clientPortField, c);

    c.gridx = 0;
    c.gridy = 3;
    c.anchor = c.NORTHEAST;
    c.fill = c.NONE;
    c.gridwidth = 2;
    c.gridheight = c.REMAINDER;
    c.weighty = 1;
    panou.add(btnConectareServer, c);

    return panou;
  }
}

// vim:set fo=wancroql tw=80 ts=2 sw=2 sts=2 sta et ai cin ff=unix:

