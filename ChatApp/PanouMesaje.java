/*
 * © 2010 ROBO Design
 * http://www.robodesign.ro
 *
 * $Date: 2010-05-19 14:00:29 +0300 $
 */

package ChatApp;

import java.awt.Color;
import java.awt.Rectangle;
import java.io.FileReader;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.text.Element;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.StyleSheet;

// clasa ce permite utilizarea facila a unui JEditorPane in taburile ce afiseaza 
// discutii.
public class PanouMesaje extends JScrollPane {
  private HTMLDocument myDoc = null;
  private Element myElemMesaje = null;
  private JEditorPane myEditorPane = null;
  private final String myFisierStil = "stil.css";

  // initializarea sablon a unui JEditorPane
  public PanouMesaje () {
    super();

    myEditorPane = new JEditorPane();
    myEditorPane.setOpaque(true);
    myEditorPane.setBackground(Color.WHITE);
    myEditorPane.setEditable(false);
    myEditorPane.setContentType("text/html; charset=utf8");
    myEditorPane.setText("<html><head><meta charset='utf8'></head><body id='mesaje'></body></html>");

    myDoc = (HTMLDocument) myEditorPane.getDocument();
    myElemMesaje = myDoc.getElement("mesaje");
    StyleSheet style = myDoc.getStyleSheet();

    try {
      style.loadRules(new FileReader(myFisierStil), null);
    } catch (Exception ex) {
      System.err.println("Nu am reusit sa aplic stilul la afisarea de mesaje. " + ex);
      ex.printStackTrace();
    }

    this.setViewportView(myEditorPane);
  }

  public JEditorPane getMyEditorPane () {
    return myEditorPane;
  }

  // metoda ce permite adaugarea rapida a unui mesaj
  public void adaugaMesaj (String htmlStr, String cssClass) {
    Element ultimulElem = myElemMesaje.getElement(myElemMesaje.getElementCount()-1);

    try {
      myDoc.insertAfterEnd(ultimulElem, "<p class='" + cssClass + "'>" + htmlStr + "</p>\n");
    } catch (Exception ex) {
      System.err.println("Afisarea de mesaje a esuat. " + ex);
      ex.printStackTrace();
      return;
    }

    myEditorPane.scrollRectToVisible(new Rectangle(0, myEditorPane.getHeight()+1000, 1, 1));
  }
}

// vim:set fo=wancroql tw=80 ts=2 sw=2 sts=2 sta et ai cin ff=unix:
